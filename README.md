Website
=======
https://www.libsdl.org/projects/SDL_ttf/

License
=======
zlib license (see the file source/COPYING)

Version
=======
2.0.12

Source
======
https://hg.libsdl.org/SDL_ttf/ (branch: SDL-1.2, changeset: 194:1ae8bba8f1d5)

Requires
========
* Freetype
* SDL1